import { login } from '../utils/request'
let actions = {
  startToken ({ commit }, { data, notify, router }) {
    login(data).then((res) => {
      if (res.success === 0) {
        commit('saveToken', res.token)
        commit('saveName', res.user.name)
        localStorage.setItem('token', res.token)
        localStorage.setItem('userName', res.user.name)
        notify({
          message: '登录成功',
          title: '用户名和密码正确',
          type: 'success'
        })
        setTimeout(() => {
          router.push({
            path: '/home'
          })
        }, 1000)
      } else {
        notify({
          message: '登录有误',
          title: '用户名和密码有误',
          type: 'error'
        })
      }
    })
  }
}
export default actions