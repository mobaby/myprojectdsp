
let mutations = {
  saveToken (state, payload) {
    state.token = payload
  },
  saveName (state, payload) {
    state.userName = payload
  },
  passphoto (state, payload) {
    state.photo = payload
    console.log(payload)
  }
}
export default mutations