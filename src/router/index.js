import Vue from 'vue'
import Router from 'vue-router'
import Login from '../views/Login'
import Home from '../views/home/home.vue'
import Data from '../views/data/data.vue'
import Utilsbox from '../views/utilsbox/utilsbox.vue'
import Create from '../views/create/create.vue'
import Plan from '../views/plan/plan.vue'
import Module from '../views/module/module.vue'
import Setadver from '../views/setadver/setadver.vue'
import Setplanone from '../views/setplanone/setplanone.vue'
// import Addcreate from '../views/Addcreate/Addcreate.vue'
import Firstcreate from '../views/firstcreate/firstcreate.vue'
import Modulecreate from '../views/modulecreate/modulecreate.vue'
import Advermodule from '../views/advermodule/advermodule.vue'
import Newbuildplan from '../views/newbuildplan/newbuildplan.vue'
import store from './../store/store'
Vue.use(Router)

let router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      redirect: '/home'
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/home',
      name: 'Home',
      component: Home
    },
    {
      path: '/data',
      name: 'Data',
      component: Data
    },
    {
      path: '/utilsbox',
      name: 'Utilsbox',
      component: Utilsbox
    },
    {
      path: '/plan',
      name: 'Plan',
      component: Plan
    },
    {
      path: '/module',
      name: 'Module',
      component: Module
    },
    {
      path: '/create',
      name: 'Create',
      component: Create
    },
    {
      path: '/setadver',
      name: 'Setadver',
      component: Setadver,
      children: [
        {
          path: 'firstcreate',
          name: 'Firstcreate',
          component: Firstcreate
        },
        {
          path: 'setplanone',
          name: 'Setplanone',
          component: Setplanone,
          redirect: '/setadver/setplanone/advermodule',
          children: [
            {
              path: 'advermodule',
              name: 'Advermodule',
              component: Advermodule
            },
            {
              path: 'newbuildplan',
              name: 'Newbuildplan',
              component: Newbuildplan
            }
          ]
        },
        {
          path: 'modulecreate',
          name: 'Modulecreate',
          component: Modulecreate
        }
      ]
    }
  ]
})
router.beforeEach((to, from, next) => {
  let islogin = routerprovder()
  if (to.name === 'Login') {
    next()
  } else {
    if (!islogin) {
      next({
        name: 'Login'
      })
    } else {
      next()
    }
  }
})
function routerprovder () {
  let providerRouter = localStorage.getItem('token')
  if (providerRouter) {
    store.commit('saveName', localStorage.getItem('userName'))
  }
  return !!providerRouter
}

export default router
