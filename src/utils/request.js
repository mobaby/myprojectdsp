import Vue from 'vue'
import axios from 'axios'

let instance = axios.create({
  headers: {
    'plantForm': 'PC'
  },
  baseURL: 'http://localhost:9000'
})
instance.interceptors.request.use(function (config) {
  config.headers.token = localStorage.getItem('token') || ''
  return config
}, function (err) {
  return Promise.reject(err)
})
instance.interceptors.response.use(function (response) {
  if (response.status === 200) {
    return response.data
  } else {
    return Promise.reject({
      status: response.status
    })
  }
}, function (err) {
  return Promise.reject(err)
})

Object.defineProperty(Vue.prototype, '$http', {
  value: instance
})
export function login (loginInfo) {
  return new Promise((resolve, reject) => {
    instance.post('/dsp-admin/user/login', loginInfo).then(res => {
      resolve(res)
    })
  })
}
export default instance