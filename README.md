# 智能业务支持系统

------

## 1. 整体技术选型

> * 首先整体选用了Vue框架(vue的核心库只关注视图层，核心是一个响应的数据绑定系统，使用起来比reactjs 简单方便多了，代码也清晰，同时也借鉴了angularjs的特点)。使用Vue脚手架(vue-cli)。
> * 样式方面使用: css , less(less是基于javascript运行的。能够嵌套样式，方便，可以实现多重继承，完全兼容 CSS 代码，可以方便地应用到老项目中)。
> * js语法使用: es6语法(es6允许从原有的对象或数组中提取值，并且对新的变量进行赋值，使用箭头函数 peomise等等)。
> * 视图层方面: element-ui(常用业务组件全面，功能丰富，团队支持较强)， echarts。
> * 数据方面: axios， mockjs，vuex。

## 2. 路由搭建，路由挂载的实现，权限接口设计
> * 登录注册
> * 新建广告
  - 新建广告计划
  - 新建广告单元
  - 新建广告创意
> * 首页概览
> * 广告管理 
  - 广告计划
  - 广告单元
  - 广告创意
> * 数据中心
> * 工具箱
  - 账号管理
  - 客户管理

> * 路由搭建： 首先进入登录页，如果登陆成功进入首页概览
> * 路由挂载的实现:在main.js中引入路由并挂载到vue的实例上，在router/index.js中写路由。
> * 权限接口设计: 在写路由的组件中，注册全局守卫router.beforeEach，在登录时用来判断登录情况，如果localStorage中有token字段，则路由直接跳转到首页概览，否则在路由拦截下重新登录
    
## 3. 数据请求以及如何封装全局请求接口
   > * 数据用mockjs生成
   > * 使用axios.create向请求头中带入headers,baseURL等
   > * http request 请求拦截器
   > * http response 服务器响应拦截器，拦截错误
   > * 登录页，需要使用封装的登录接口，将用户名和密码传入后台，后台判断是否存在此时输入的用户名和密码，如果存在，返回token字段，否则重新登录
   > * 首页概览，echarts图表需要用到接口,需要请求接口获得数据,将需要的数据用mockjs mock出来(横纵坐标需要不同的数据)，操作日期,将开始日期与结束日期包括的月份作为横坐标。使日历的选择与图表变化联系起来。通过改变日期来重新改变数据并渲染视图。如果登录,则将用户名信息显示通过mapState映射在首页概览中
   > * 在广告创意页获取到所有输入的信息,将信息放进后台管理,通过请求接口在广告创意页面中取得这些数据,并渲染到页面。
   
   
## 4. 项目中的核心功能和难点功能介绍
  > *【核心功能】: 1.登录时输入用户名和密码，判断后台是否有此token字段，                   有则登录成功，跳转到首页，否则重新登录
                2.首页概览中的echarts图表与日期结合，通过改变日期使图                    表发生更新。
                3.      上传图片，使用element-ui中的upload组件，下载multer使用，组件中input的name要与接口中upload.single('')名保持一致。 =>upload.single('与input的name值对应'),将取到图片的路径,尺寸响应到前端。。自己封装也可使用formdata,通过onchange事件获得图片信息。
  > *【难点功能】: tab切换功能，需要点击添加创意动态添加创意element-ui可                 实现切换，但在添加时有问题。自己封装tab切换，准备空                  数组，点击添加创意时向数组里push内容，并在子组件中渲                 染出来，需要使用slot存放每个tab下的内容
  
## 5. 数据管理
  > * 主要运用的是vuex来管理数据
    - 需要在main.js中引用store，并挂载在实例上
    - 在state里定义我们需要的内容，在mutations给之前在state中赋值payload。在action里通过commit给mutations中的函数传去对应的数据。最后在视图上通过this.$store.dispatch()触发视图。
    - 在登录页面需要将token字段放进store管理。
    - 在首页中需要将用户名信息放进store管理。
    
## 6. 遇到的困难，以及解决方式
> * tab切换点击添加创意需要自己手动封装，准备空                  数组，点击添加创意时向数组里push内容，并在子组件中渲                 染出来，需要使用slot存放每个tab下的内容。
> * 点击不同的创意，不同创意下的内容都在同一个盒子里。在别的创意下上传了图片，之前的图片就不存在了。将上传图片单独封装一个组件，在父组件中引入此组件，同时点击哪个创意，就让对应的内容显示。

![cmd-markdown-logo](http://on-img.com/chart_image/5ad8009fe4b0f5fa24e4f234.png)
